\documentclass[portrait]{tikzposter} % [margin=4cm, paperwidth=84.1cm, paperheight=118.9cm]

\usetheme{Envelope} % Basic}
% \usecolorstyle{Spain}
% \useblockstyle{Default}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}

\usepackage{mathtools} 
\usepackage{amsfonts, amsmath, amssymb, amsthm}
\usepackage{enumerate}
\usepackage{mathrsfs}           % for mathscr
% \usepackage{mathabx}            % for widebar
% \usepackage{dsfont}    % Font
\usepackage{multicol}

\usepackage[backend=bibtex]{biblatex}
\addbibresource{bibliography.bib}

% Graphics
\usepackage{graphicx}           % for scalebox

% Customized hyper references
\usepackage[pdftex,colorlinks]{hyperref}
\hypersetup{                    %
            citecolor=blue,     %
            filecolor=blue,     %
            linkcolor=blue,     %
            urlcolor=blue}
\usepackage[noabbrev,capitalize]{cleveref} % after hyperref
\usepackage{caption}
\include{definitions}
\include{fancy}

\author{Gianmarco Brocchi \quad supervised by \quad Diogo Oliveira e Silva}
% \email{gbb707@bham.ac.uk}
% \usetitlestyle{Simple}

\title{\parbox{\linewidth}{\centering \bfseries Fourth order Schrödinger equation and Strichartz estimates: \\ an extreme adventure}}
% \title{\bfseries Fourth order Schrödinger equation and Strichartz estimate: an extremal adventure}
\institute{University of Birmingham}

\begin{document}
\maketitle
\begin{columns}
  \column{.5}
  \block{Schrödinger waves \& Strichartz estimates}%
  {
    \coloredbox{colorone!50!}{      
      \begin{minipage}[c]{.2\linewidth}
          \includegraphics{Erwin}
          \captionof{figure}{Erwin Schrödinger}
      \end{minipage}\hspace{2cm}
      \begin{minipage}{.7\linewidth}
        The evolution of a quantum system
        % \footnote{with null potential and initial state $u_0$}
        is described by the
        solution of the Schrödinger equation $u(t,x)$:
        \begin{equation*}
          \begin{cases} \label{eq:FreeSE} % \tag{SE}
            i \partial_t u(t,x) = \partial_x^2 u(t,x) \qquad x,t \in \R \\
            u(0,x) = u_0(x) % \,,\quad u_0 \in \Schwartz(\R^d).
          \end{cases}
        \end{equation*}
        where $i^2 = -1$. The solution $u$ % is called wave function, and
        is 
        
        \coloredbox{colorthree!50!}{
          \[ u(t,x) = e^{-it\partial_x^2}u_0 = \frac{1}{2 \pi} \int_{\R} e^{i(x \xi + t \xi^2)}\widehat{u_0}(\xi) d \xi \]
        }
        where $ \hat{f}(\xi) := \int_{\R} e^{-ix \xi} \, f(x) d x $
        is the Fourier Transform.
      \end{minipage}\vspace{.5cm}

      \begin{itemize}
      \item This is a \emph{dispersive} equation:
        its solutions spread out in space as time evolves.
      \end{itemize}
    }

    \coloredbox{colorthree!50!}{          % Strichartz estimates
      \noindent To measure dispersion we integrate % powers of the solutions
      in time and space, estimating a mixed norm:
      \begin{equation} \label{eq:strichartz} \tag{Strichartz}
        \scalebox{1.2}{$\lVert e^{-it\partial_x^2} u_0 \rVert_{L^q_t(\R;L^p_x(\R))} \leq C \, \norm{u_0}_{L^2(\R)}$}
      \end{equation}

      \begin{minipage}{.65\linewidth}        
        \noindent %
        \begin{minipage}[t]{0.4\linewidth}
          % By scaling \eqref{eq:strichartz}, we obtain the condition:  
          \vspace{1cm}          
          \begin{equation*}
            \text{where} \quad \frac 2 q + \frac 1 p = \frac 1 2
          \end{equation*}

          \vspace{.5cm}
          
          \begin{equation*}
            \text{ and } \quad  2 \leq p \leq \infty .
          \end{equation*}                    
        \end{minipage}\hspace{2cm}
        \begin{minipage}[t]{.4\linewidth} \vspace{-5mm}
          \begin{tikzfigure}
            \begin{tikzpicture}[domain=0:5,scale=3] 
              \draw[->] (0,0) -- (2.5,0) node[right] {$\frac 1 p$}; 
              \draw[->] (0,0) -- (0,1.5) node[right] {$\frac 1 q$};
              % x axes
              \node (punto) at (2,0)[label=above:$\frac 1 2$] {$\bullet$};
              \node (punto) at (0.66,0.66)[label=above:${\color{blue}\frac 1 6}$] {${\color{blue}\bullet}$};
              % y axes
              \node (punto) at (0,1)[label=left:$\frac 1 4$] {$\bullet$};          
              % linee
              \draw (2,0) -- (0,1);
              \draw[dashed, color=blue] (0,0) -- (1.2,1.2);
            \end{tikzpicture}
            \captionof{figure}{Admissible $(p,q)$ for \eqref{eq:strichartz}}
          \end{tikzfigure}
        \end{minipage}\vspace{1cm}

        \noindent These estimates %, called Strichartz Estimates,
        are a fundamental tool in proving well-posedness
        of the Nonlinear Schrödinger Equation via fixed point theorems.
      \end{minipage}\hspace{1cm}
      \begin{minipage}[c]{.3\linewidth}
        \tikz[baseline=(a.north)]\node[xscale=-1,inner sep=0,outer sep=0](a){\includegraphics[height=13cm]{Robert}};
        \captionof{figure}{Robert Strichartz}
      \end{minipage}
    }
    
  \innerblock{Extremizers}{    
      \begin{define*}
        An \emph{extremizing sequence} is a sequence $\{f_n\}_{n \in \N}$ in the unit ball of $L^2$ such that
        \begin{equation*}
          \lim_{n \to \infty} \norm{ e^{-it\partial_x^2} f_n}_{L_t^q(\R)L_x^p(\R)} \to \mathbf{C} .
        \end{equation*}
      \end{define*}

      \begin{remark}
        Extremizing sequences may not converge! 
      \end{remark}
      
      \begin{define*}
        An \emph{extremizer} is a function $f \neq 0$ that realises equality in an inequality.
      \end{define*}      
    }
  }

  \block{4\textsuperscript{th} order Schrödinger equation} {
    \coloredboxw{\linewidth}{colorone!50!}{
      Let's focus on the fourth order equation:
      \begin{equation*}\label{eq:intro family}\tag{1}
        i \partial_t u(t,x) + \partial_x^{{\color{red}4}} u(t,x) = 0 , \quad x,t \in \R
      \end{equation*}    
      The solution with initial datum $f \in L^2(\R)$ is 
      \begin{equation*}
        S(t) f := e^{i t \partial_x^4}f = \frac{1}{2\pi} \int_\R e^{i(x \xi + t \xi^4)}\hat{f}(\xi) d \xi \, .
      \end{equation*}    
      We have the Strichartz estimate:

      \begin{center}
      \coloredboxw{.6\linewidth}{colorthree!50!}{
        \begin{equation*}\label{eq:ourstrichartz}\tag{$\star$}
          \norm{\partial^{\frac13} e^{i t \partial^4} f }_{L_{t,x}^6(\R \times \R)} \leq \SharpC  \Norm{f}_{L^2(\R)} .
        \end{equation*}
      }
    \end{center}
    \vspace{.5cm}
    
    Where $\partial^{\frac13} f (x) := \frac{1}{2\pi} \int e^{ix\xi} \abs{\xi}^{\frac{1}{3}} \hat{f}(\xi) d \xi $, and $\SharpC$ is the best constant.
    }

    Let $\mathbf{C}$ be the best constant in \eqref{eq:strichartz}, when $q = p = 6$.
    By a result in \cite{jiang2010linear}, if $\mathbf{S} > \mathbf{C}$ then extremizers for \eqref{eq:ourstrichartz} exist. 

    \vspace{.5cm}
    
    % To show existence of extremizers is enough to show that $ \mathbf{S} > \mathbf{C}$ .\\

    Using the Even Trick with $w(\xi) := \abs{\xi}^{\frac{2}{3}}$ and $\rho := \ddelta{\tau - \xi^4}$ we have
    \begin{equation*}
      \norm{\partial^{\frac13} e^{i t \partial^4} f }_{L_{t,x}^6(\R \times \R)}^3 = (2\pi)^{-2} \norm{ \hat{f} \sqrt{w} \rho * \hat{f} \sqrt{w} \rho  * \hat{f} \sqrt{w} \rho }_{L_{t,x}^2}.
    \end{equation*} \vspace{.2cm}
    % and using Plancherel
      
    \innerblock{Convolution of singular measures II}{
      \noindent \begin{minipage}{0.45\linewidth}
        This time \\ $ \ds \supp(\rho)~\subset Q~=~\set{(\tau,\xi)\,:\,\tau=\xi^4}$,        
        and the support of the convolution $\rho * \rho * \rho$ is
        \begin{equation*}
          \set{(\tau,\xi) \in \R^2\, : \, 3^3 \tau\ge\xi^4 }.
        \end{equation*}
        Moreover, its Radon-Nikodym derivative is
        radial and constant along 
        branches of quartics $\tau = \alpha \xi^4$.
        Its value at a point depends only on
        the aperture $\alpha$ of the quartic through that point.
      \end{minipage}\hspace{-1cm}
        \begin{minipage}{0.65\linewidth}
          \begin{tikzfigure}
            \begin{tikzpicture}[scale=2]
              \begin{scope}             
                \clip (-5,0) -- (5,0) arc(0:180:5) --cycle;
                % base of the support
                \draw[dashed, scale=2,domain=-3:3,smooth,variable=\x,black,fill=lightgray!40] plot ({\x},{.037*(\x)^4});
                
                % sum of (some) quartic
                \draw[scale=2,domain=-2:2,smooth,variable=\x,black] plot ({\x},{(\x)^4});
                \draw[scale=2,domain=-2:1,smooth,variable=\x,black] plot ({\x},{(\x+.6)^4+.1});
                \draw[scale=2,domain=-4:0,smooth,variable=\x] plot ({\x},{(\x+1.3)^4+.4});
              \end{scope}
              
              % Different amplitudes on the right
              \begin{scope}    
                \clip (-4.5,0) -- (4.5,0) arc(0:180:4.5) --cycle;
                \foreach \a in {.08,.2,.6,7}
                \draw[purple,scale=2,domain=0:2,smooth,variable=\x] plot ({\x},{\a*(\x)^4});
              \end{scope}

              \draw[->, thin, dotted] (-4,0) -- (4,0) node[below] {$\xi$};   % x 
              \draw[->, thin, dotted] (0,0) -- (0,4) node[right] {$\tau$};        % y

              \draw[purple] (3.6,.7) node[below]{$\alpha$};
              \draw[purple, dotted, thick, ->] (3.4,.7) arc (40:85:4.5) ;

              % \draw (-2.5,4.3) node[above]{$\{ \tau \geq 3^{-3} \abs{\xi}^4 \}$};
            \end{tikzpicture}
          \end{tikzfigure}
          \captionof{figure}{Support of the measure $\rho * \rho * \rho$}
        \end{minipage}  
      }
    }
    
  \column{.5}
  \block{Restriction\textsuperscript{$\star$} theory \& Even Trick}{
    The space-time Fourier transform of $u$

    \coloredbox{colorthree!50!}{      
      \begin{equation*}
        \Fourier(u)(\tau,\xi) = \int_{\R \times \R} e^{-i(x \xi + \tau t)} u(t,x) d x dt
      \end{equation*}
    }
    is supported on the parabola $P = \{(\tau,\xi) \in \R^{2}, \tau~=~\xi^2 \}$.
    \vspace{1cm}
    
    \noindent \begin{minipage}[t]{.6\linewidth}
    % Estensione dalla parabola
    \begin{tikzfigure}      
      \begin{tikzpicture}[scale=3.5, line width=4] %, >=latex]
        % Frequency realm
        \draw[opacity=.1,fill=green] (-2.3,-.5) rectangle (2.3,2.3);
        \draw[opacity=.4,->, line width=2] (-1.5,0) -- (1.5,0) node[right] {$\xi$}; % x 
        \draw[opacity=.4,->, line width=2] (0,-.2) -- (0,1.5) node[left] {$\tau$};   % y
        
        % Initial datum
        \draw[opacity=.4,->, line width=3] (-3,-1.5) -- (-1.5,-1.5) node[right] {$x$}; % x 
        \node (dot) at (-2,-1.5) [label=above left:$u_0$]{};

        % parabola
        \draw[line width=3,scale=.5,blue,domain=-2:2,smooth,variable=\x] plot ({\x},{(.8)*(\x)^2});

        % % sol operator
        \draw[line width=4, |->] (-.5,-1.1) -- (1,-1.1) node[above left]{$e^{it\partial^2}$};
        % \node (dot) at (0,-1) [label=above:$u_0 \mapsto e^{it\Delta}u_0$]{};

        % 1. Hat
        \draw[color=teal,->] (-2.4,-.8) to [out=80,in=220] node[scale=1, left] {\circled{1}} (-2,-.2); % node[left]{$\widehat{\cdot}$}; 
        \node (dot) at (-1.4,.1) [label=left:$\widehat{u_0}$]{};  
        % Lift on the parabola
        \draw [->, cyan] (-1,.2) to [out=90,in=210] node[scale=1, left] {\circled{2}} (-.8,.6);
        \draw [->, cyan] (1,.2) to [out=90,in=-30] (.8,.6);
        % 2. Lift
        \node (dot) at (.2,1.9) [label=right:${\color{blue}\widehat{u_0} \, \ddelta{\tau - \xi^2}}$]{};
        % 3. Space-time Fourier
        \draw [<-, purple] (3.4,-.5) to [out=90,in=-30] node[scale=1, right] {\circled{3}} (2.2,1.6);
        \node (dot) at (2.3,.6) [label=right:${\color{purple}\mathcal{F}^{-1}}$]{};
        
        % Right
        % Space-time realm
        \draw [opacity=.1,fill=brown] (1.5,-2) rectangle (4,-.4);

        \draw[opacity=.4,->, line width=2] (2,-1.5) -- (3.4,-1.5) node[right] {$x$}; % x
        \draw[opacity=.4,->, line width=2] (2.5,-1.7) -- (2.5,-.8) node[right] {$t$};   % y
        \node (dot) at (2.5,-1.5) [label=above left:$u$]{};
      \end{tikzpicture}
    \end{tikzfigure}
  \end{minipage}\hspace{2cm}
  \begin{minipage}[t]{.3\linewidth}
    In fact, we can write $u(t,x)$ as the inverse space-time Fourier transform
      of a measure supported on $P$, like $\sigma := \ddelta{\tau - \xi^2}$.    
    % is a singular measure supported on the parabola.

    \vspace{1cm}
    
    This operation is called \emph{Fourier extension}.
    It is the adjoint operator of the \emph{Fourier Restriction}. 
  \end{minipage} \vspace{1cm}

    The propagator $e^{-it\partial^2}$ is the Fourier extension (from $P$) of the measure $\widehat{u_0} \sigma$
    \begin{align*}
      u_0 & \xmapsto{\phantom{ \widehat{u_0} \xmapsto{\circled{2}} \widehat{u_0} \hspace{2cm}} \scalebox{1.2}{$e^{-it\partial^2}$} \phantom{\sigma \xmapsto{\circled{3}} \Fourier^{-1}(\widehat{u_0} \, \sigma) \hspace{14mm}} } u(t,x) \\
      u_0 & \xmapsto{{\color{teal}\circled{1}}} \widehat{u_0} \xmapsto{{\color{cyan}\circled{2}}} \widehat{u_0} \, \sigma \xmapsto{{\color{purple}\circled{3}}} \Fourier^{-1}(\widehat{u_0} \, \sigma) = u(t,x)
    \end{align*}
    
    % \begin{equation*}
    %   u = \Fourier^{-1}(\underbrace{\widehat{u_0}(\xi)}_{f(\xi)} \, \underbrace{2 \pi \, \delta(\tau - \abs{\xi}^2)}_{\mu(\tau,\xi)}) = \Fourier^{-1}(f \, \mu).\label{eq:sol_as_restriction}
    % \end{equation*}    

    \innerblockplain[colorone!80!]{ % Even trick
      \paragraph{Reduce the $L^{6}$-norm to $L^2$-norm} Raise the norm to power $3$
      \begin{equation*}
        \norm{ u }_{L_t^6(\R)L_x^6(\R)}^3 =  \norm{ \abs{u}^3 }_{L_t^2(\R)L_x^2(\R)}  
      \end{equation*}
      
      then apply Plancherel using the space-time Fourier Transform:
    \begin{equation*}
      (2\pi)^3 \, \norm{ u \cdot u \cdot u }_{L^2(\R_t \times \R_x)} = \norm{ \Fourier(u) * \Fourier(u) * \Fourier(u) }_{L^2(\R_\tau \times \R_\xi)} = \norm{ \widehat{u_0} \sigma * \widehat{u_0} \sigma * \widehat{u_0} \sigma }_{L^2(\R_\tau \times \R_\xi)} .
    \end{equation*}
    % \begin{equation*}
    %   u = \Fourier^{-1}(\underbrace{2 \pi \, \widehat{u_0}}_{f(\xi)} \, \underbrace{\delta(\tau - \xi^2)}_{\sigma(\xi,\tau)}) = \Fourier^{-1}(f \, \sigma).\label{eq:sol_as_restriction}
    % \end{equation*}
    The problem reduces to estimating the $L^2$-norm of (3-fold) convolutions of the weighted measure $\widehat{u_0} \sigma$ with itself.
  }  
    \innerblock{Convolution of singular measures}{
      \begin{minipage}{.5\linewidth}
        The support of the (3-fold) convolution $\sigma * \sigma * \sigma$ is
        \begin{equation*}
          \overline{P + P + P} = \set{(\tau,\xi) \, : \, 3\tau\ge\xi^2 }.
        \end{equation*}
        This is because $\supp(\sigma) \subset P$ and
      \begin{equation*}
        \supp(\sigma * \sigma) \subseteq \overline{ \supp(\sigma) + \supp(\sigma) } ,
      \end{equation*}
      and so
      \begin{equation*}
        \supp(\sigma * \sigma * \sigma) \subseteq  \overline{P + P + P}.
      \end{equation*}
    \end{minipage} \hspace{-4cm}
    \begin{minipage}{.5\linewidth}
      \begin{tikzfigure}
        \begin{tikzpicture}[scale=4, >=latex]
          \clip (-3,-.5) -- (3,-.5) arc(0:180:3) --cycle;
          \fill[black!10]
          (-3, 3) parabola[bend at end] (0,0) parabola (3, 3) -- cycle;
          \draw[dashed]
          (-3, 3) parabola[bend at end] (0,0) parabola (3, 3);
          \draw[->, thin, dotted] (-2, 0) -- (2, 0) node[below] {$\xi$};
          \draw[->, thin, dotted] (0, -0.2) -- (0, 2) node[left] {$\tau$};
          \draw
          (-1.4, 1.96) parabola[bend at end] (0,0) parabola (1.4, 1.96);
          \draw[shift={(0.4,0.16)}]
          (-1.3, 1.69) parabola[bend at end] (0,0) parabola (1.3, 1.69);
          \draw[shift={(1,0.52)}]
          (-1.1, 1.21) parabola[bend at end] (0,0) parabola (1.1, 1.21);

          \fill[black!50, text=black]
          (0, 0) circle (1pt) node[below left] {$O$}
          (0.4, 0.16) node (a) {} circle (1pt)
          % (0.8, 0.64) node (b) {} circle (1pt)
          (1,0.52) node (c) {} circle (1pt);

          \path (0.6, 1.6) node {$3\tau \ge \xi^2$};
        \end{tikzpicture}

      \end{tikzfigure}
    \end{minipage}
    }
  }
  
  \block{Existence of Extremizers}{%
    \coloredbox{colorthree!60!}{%
      \begin{teo*}
        Extremizers for the Strichartz inequality \eqref{eq:ourstrichartz} exist. 
      \end{teo*}
    }
    
    \coloredbox{colorone!80!}{%
      % \begin{proof}[Sketch of the proof]      
      Consider $f(x) = e^{-x^4} \sqrt{w(x)}$. Then
        \begin{equation*}
          \norm{f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu}^2_{L^2(\R^2)} = \int_{\R^2} e^{-2\tau} (w\nu *w\nu *w\nu)^2(\xi,\tau) d\xi d\tau ,
        \end{equation*}
        We can also compute the $\norm{f}_2$ explicitly.
        Changing variables and exploiting the homogeneity we obtain
        \begin{equation*} \label{eq:quantity_with_coefficients}
          2\pi \, \SharpC^6 \geq \frac{\norm{f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu}^2_{L^2(\R^2)}}{\norm{f}_{L^2(\R)}^6} = c_0 \, \int_{-1}^1 g^2(t) d t .
        \end{equation*}
        where $g(t) = (w\nu *w\nu *w\nu)(1,3^{-3}t^{-4})$ and $c_0 = \frac{2^3 3^\frac34  \Gamma\pa{\frac54}}{\Gamma\pa{\frac{5}{12}}^3}$.

        % Approximate $\norm{g}_{L^2}^2$ with
        Writing $g$ in the basis of Legendre polynomials $g = \sum c_n L_n$ gives
    \begin{equation*}
      \norm{g}_{L^2}^2 = \sum_{n \geq 0} c_{2n}^2 \geq c_0^2 + c_2^2 + c_4^2 \approx 0.306879 > \frac{\pi}{6 \sqrt{3}}.
    \end{equation*}    
    This lower bound for $\SharpC$ is good enough to ensure that $\SharpC > \mathbf{C}$.
  % \end{proof}
  }
}


%  \getcurrentrow{note}

  %% a plain block
  %% #1 - rotate angle (optional), #2 - where,
  %% #3 - width, #4 - title, #5 - text
  % %%%%%%%%%% ------------------------------------------ %%%%%%%%%%
  % \plainblock{($(currentrow)+(xshift)-(yshift)$)}%[($(currenty)+(0,10)$)]%
  % {32}{Plain blocks} %
  % {These blocks are similar to callout blocks. They allow for specifying the
  %   title of the block.
  % }


  % Bib
\note[targetoffsetx=-3cm, targetoffsety=-18cm, radius=3cm,
width=40cm, linewidth=.2cm,
roundedcorners=30, innersep=1cm]{
  {\Large \bf{References}}
  \begin{center}\mbox{}\vspace{-\baselineskip}
    \printbibliography[heading=none]
  \end{center}
}

\end{columns}

  
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
