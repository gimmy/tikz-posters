# Maths TikZ posters

Scientific posters for presentations.

It uses the Botoeva's fancytikzposter [template](https://www.doc.ic.ac.uk/~ebotoeva/fancytikzposter.html).

## Compile
- Run `latexmk --pdf Poster.tex` to compile. If complains, ignoring and runnig it again should work.

(I know this it _not_ an example of good code practices.
   Maybe you don't want to copy this,
 and rather using something more supported, as the official TikZposter [CTAN](https://ctan.org/pkg/tikzposter), [Bitbucket](https://bitbucket.org/surmann/tikzposter/wiki/Home))



